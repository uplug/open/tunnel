FROM registry.gitlab.com/uplug/open/elixir-modules/flux_database:development

COPY config config
COPY mix.exs mix.lock ./

RUN mix deps.get
