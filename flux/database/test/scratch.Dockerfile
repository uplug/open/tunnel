FROM registry.gitlab.com/uplug/open/docker-images/elixir

ENV MIX_ENV=test

RUN mkdir -p /path/to/storage

COPY config config
COPY lib lib
COPY test test
COPY .formatter.exs mix.exs mix.lock ./

RUN mix deps.get && \
    mix deps.compile

CMD [ "sleep", "infinity" ]
