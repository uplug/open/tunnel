FROM registry.gitlab.com/uplug/open/elixir-modules/flux_database:test

COPY config config
COPY lib lib
COPY test test
COPY .formatter.exs mix.exs mix.lock ./

RUN mix deps.get
