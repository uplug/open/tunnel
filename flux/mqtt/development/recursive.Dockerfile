FROM registry.gitlab.com/uplug/open/elixir-modules/flux_mqtt:development

COPY config config
COPY mix.exs mix.lock ./

RUN mix deps.get
