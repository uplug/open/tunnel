FROM registry.gitlab.com/uplug/open/elixir-modules/flux_influxdb:test

COPY config config
COPY lib lib
COPY test test
COPY .formatter.exs mix.exs mix.lock ./

RUN mix deps.get
