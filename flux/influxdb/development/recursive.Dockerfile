FROM registry.gitlab.com/uplug/open/elixir-modules/flux_influxdb:development

COPY config config
COPY mix.exs mix.lock ./

RUN mix deps.get
