FROM registry.gitlab.com/uplug/open/docker-images/elixir

COPY config config
COPY mix.exs mix.lock ./

RUN mix deps.get && \
    mix deps.compile && \
    MIX_ENV=test mix deps.compile

CMD [ "sleep", "infinity" ]
